# Maintainer: Noel Kuntze <noel.kuntze@contauro.com>
pkgname=grommunio-gromox
pkgver=2.28
pkgrel=0
pkgdesc="Open Source Groupware Solution"
arch="all !riscv64 !s390x" # test failure on s390x
url="https://grommunio.com/"
license="AGPL-3.0-or-later"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev $pkgname-openrc"
_php=php83
install="$pkgname.pre-install $pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
depends="
	grommunio-common
	mariadb
	$_php
	$_php-fpm
	postfix
	valkey
	!kopano-core
"

makedepends="
	autoconf
	automake
	curl-dev
	gumbo-parser-dev
	jsoncpp-dev
	krb5-dev
	ldns-dev
	libhx-dev
	libtool
	libvmime-dev
	libxml2-dev
	linux-pam-dev
	linux-headers
	mariadb-dev
	musl-dev
	openldap-dev
	openssl-dev
	$_php
	$_php-dev
	sqlite-dev
	tinyxml2-dev
	xxhash-dev
	zlib-dev
	zstd-dev
"

pkgusers="grommunio gromox"
pkggroups="grommunio gromox gromoxcf"

source="
	https://github.com/grommunio/gromox/archive/refs/tags/gromox-$pkgver.tar.gz
	0001-syslog-ident.patch

	gromox-delivery-queue.initd
	gromox-delivery.initd
	gromox-event.initd
	gromox-http.initd
	gromox-imap.initd
	gromox-midb.initd
	gromox-pop3.initd
	gromox-timer.initd
	gromox-zcore.initd
	gromox-fpm.conf
	delivery.cfg
	event.cfg
	http.cfg
	imap.cfg
	midb.cfg
	mysql_adaptor.cfg
	pop3.cfg
	smtp.cfg
	timer.cfg
	zcore.cfg
	autodiscover.ini
	grommunio-virtual-mailbox-domains.cf
	grommunio-virtual-mailbox-alias-maps.cf
	grommunio-virtual-mailbox-maps.cf
	grommunio-bcc-forwards.cf
"

builddir="$srcdir/gromox-gromox-$pkgver/"

prepare() {
	default_prepare
	autoreconf -fiv
}

build() {
	export CXXFLAGS="$CXXFLAGS -fpermissive"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var

	make
}

check() {
	make check
}

package() {
	mkdir -p "$pkgdir"
	make install DESTDIR="$(realpath $pkgdir)"

	# prepare services
	for item in delivery-queue delivery event http imap midb pop3 timer zcore; do
		install -Dm 0755 "$srcdir"/gromox-$item.initd "$pkgdir"/etc/init.d/gromox-$item
	done

	# prepare configuration. some files are predefined, others are generated.
	install -dm 0750 "$srcdir/gromox"
	for item in delivery event http imap midb mysql_adaptor pop3 smtp timer zcore; do
		install -Dm 0640 "$srcdir"/$item.cfg "$pkgdir"/etc/gromox/$item.cfg
	done

	# autodiscover.ini
	install -Dm 0640 "$srcdir"/autodiscover.ini "$pkgdir"/etc/gromox/autodiscover.ini

	# php-fpm
	install -Dm 0644 "$srcdir"/gromox-fpm.conf "$pkgdir"/etc/$_php/php-fpm.d/gromox.conf
	rm "$pkgdir"/usr/share/gromox/fpm-gromox.conf.sample

	# postfix files
	install -Dm 0640 -g postfix "$srcdir"/grommunio-bcc-forwards.cf "$pkgdir"/etc/postfix/grommunio-bcc-forwards.cf
	install -Dm 0640 -g postfix "$srcdir"/grommunio-virtual-mailbox-alias-maps.cf "$pkgdir"/etc/postfix/grommunio-virtual-mailbox-alias-maps.cf
	install -Dm 0640 -g postfix "$srcdir"/grommunio-virtual-mailbox-domains.cf "$pkgdir"/etc/postfix/grommunio-virtual-mailbox-domains.cf
	install -Dm 0640 -g postfix "$srcdir"/grommunio-virtual-mailbox-maps.cf "$pkgdir"/etc/postfix/grommunio-virtual-mailbox-maps.cf

	# license
	install -Dm 0644 LICENSE.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE

	# create database directories
	for i in /var/lib/gromox \
		/var/lib/gromox/domain \
		/var/lib/gromox/user \
		/var/lib/gromox/queue \
		/var/lib/gromox/queue/cache \
		/var/lib/gromox/queue/mess \
		/var/lib/gromox/queue/save \
		/var/lib/gromox/queue/timer; do
		install -dm 0770 -g gromox -o gromox "$pkgdir"/$i
	done

	# make sure all files in /etc/gromox are owned by grommunio:grmoxcf
	chown -R grommunio:gromoxcf "$pkgdir"/etc/gromox
	find  "$pkgdir"/etc/gromox -type f -exec chmod 0640 {} ';'
	find  "$pkgdir"/etc/gromox -type d -exec chmod 0750 {} ';'

	# remove unnecessary folders
	rm -r "$pkgdir"/usr/lib/systemd "$pkgdir"/usr/lib/sysusers.d/ "$pkgdir"/usr/lib/tmpfiles.d/

}

sha512sums="
cbe23c8a490f6d34f967f9467fd9451fc9e9ab1069f57cd0d7677a623827b7218f500dbf851bc9e0e5ccd37e116c1491d63fe0cd60cda0126673952be3cdd720  gromox-2.28.tar.gz
8b87976266c3a56b9e6cf186faa7bf2eab2c197feefc3b2482320da19e304ab75bc9f263ea821f9a484a02964ae50f7ce9ce68c834c8cc23d413525fde4662db  0001-syslog-ident.patch
d6fe6c086299e92512572b03bf4235cf1ca98d13e876dda531f302c21a77e59941cb04ac9c22062e20be9a2a33209e2f87aaf46586e97f703299ba5cec899c26  gromox-delivery-queue.initd
71ed3b5d6aad63639bb86f99ec7955f518dd039283948fa66a97a39e2dae17ecb532156d62b336e94f6c5e6acf54a77015f020b7c557e3f99584e005ad9995f7  gromox-delivery.initd
4f887564376fbbf119a20085c09f970707afa4b711f67358746bf8342f0c00526c41cc861e693b7e050723f572b3890d6882cb8d4ed2250e290a9dbb9ae7f3e3  gromox-event.initd
7bc60c2205d1e4c466e0da0c97928851cb5b25c148f90bf1bce3b8dde6a819276edc84da007e7a3bb35988d87b70464bf7bc17a20f802f8f52b679062fbf71c9  gromox-http.initd
deaf1d15a286dd6cb2989e1f14a2af27debe1239b1588fb126538c7ca0e803538e467024e9922dd38d2d8b28126676604e7cf6b6ceef0969da4a7cd6c7ca9b70  gromox-imap.initd
d179768b99dde4f86569f94630ef2e96f4c3c6c2e9f3bbcabb52335f9bb73175e0c0a67921fe99acebfd7f4dd6e39adb7f94a00b5dc5ae94789bf4e39ec2a347  gromox-midb.initd
0e6846df2fa6c3bbedac3fdefdb2f14ce5b7d534cd741ca391d39473e8843fdf447f21efa0965877f40f388660525104c26c687e2ecd7c67ea1c212fdbd4f96f  gromox-pop3.initd
ed1cdab1bf211aad2b9542a73326d42249b69a9fab8a31d5c1a62179709ee3091fbbf1988e7f62ceeda1e87007c5f8ec02ffea43172dc90d9ae9e39a5fdc322b  gromox-timer.initd
5cc6a5ede4b1f56e9ea240fb050617f89330500eba64f441154dfad0ef001a6ef26e715fd41fcd77d310ec4381749021e44660430419de319a65becfaf0c9be0  gromox-zcore.initd
adb0da75223284a1081166597ef42d267d2187f663b7da7c66eae517e0af179217464a1e7ec6b2d082f7dd0b506e1e980724cf1fec081719b253e8ea44a37558  gromox-fpm.conf
e610fd0cb1392f08cd5cd30450fc9d6cdd6a2724ff439a38a2c6623755c9e2afdeee2f9d45d965ba0dd043b5c3942c1a0d69061f7cc9b1ecdd680109e7d756de  delivery.cfg
b9f4d736c41278b320bae89e5c0d6674fd5b628bc6fb6c5b88b15e37617f39d228a884988aeaba5b2fbe05a64372ed7f8094cdc363a2a1901b0c6c1735844e92  event.cfg
281a6deb683722222a34e149003e6d361ca41f45ca73d32a46a37a757cf39847e5e90868f9af454f9ee59fcf570716c011ee5087a02e705f68b0ded73c2c4c43  http.cfg
ff5f2bb87d49c8127eefd1c26e2480a9c8e9730d10e890a69b9ba2bf9b4e8c11b5841b4c8be59a6fbeb0eb3c803fb3323a1cc971c1c571ed11719e169f4745bd  imap.cfg
07997f87175bc835059d7a9c8b59fc282bb6e835c9ea34b87e7f2ec90a27d522a44f19d28b4ab9bcd009053f88756b6ebe9de7edbbde2ded24bd56c5767251db  midb.cfg
a4b54f32a4f8b932ea93dd71c1eaccac50a445d960f3ab37913481db3b80daad51238f4b826331111e939f5a75d522619fff9566042670b578c65c477754bd64  mysql_adaptor.cfg
43db0b8d9d65d5e1d86195875cc564d11a0c4d5970a571bc97dc6150d74d8f9c456adb0ac1db9cc84302923e285e2f6d9b564dfd92a8039e97e5f98834327b4a  pop3.cfg
a0cf62b2982479faa3e00ec401ae1946505eb7c581507f45757663e5e9b922a126c96e56e1a64090016420f4e045c83efffe06e725c96f9887c745b6581bbfb2  smtp.cfg
f910bae21e35f79552ae79899263ca8d1ba7bfd85944cc0915e4dda83e35600cdce00b17346d788ea3c0060fea4cd1525d5781524ce086712d51b49c6bc0cd44  timer.cfg
f1ecee83fa72137bba15e11f109c8501bbc8712c8a5fa41aab3468da05e1ef6a61c5ac03488688222a7cd4e54fa9c2e6696819471d6d8deb9193865033327f8d  zcore.cfg
91b433070e89dfc4b2d7ea9360547bc2d1dd37bd220c3e688994249d52f17d980c15f41ec52505aa027644633546a61742da3f3155c267d99bc0c37956f118f3  autodiscover.ini
8890e170ef2c9b1cc43ab84c2c24446f20be69dd395ec8249246568aaadcd440a304103a36d008da8cbf20ec8ec0522dadf2217ee4ee00255c58a442d94ec263  grommunio-virtual-mailbox-domains.cf
2ce6b5867a9dd9ef5091e1b167813a14900958be9fe5ba46bf333a58bd7b9fe3d8dfd2a25d65a1ae567dc362ec98ea648d4ae5a95f87370a083d6707c1bda475  grommunio-virtual-mailbox-alias-maps.cf
22161519cacb4f97577ca17216b9d2aa0212c32d7d7d6f3d82c8894628f4b6ab7471cf49530bebe22f1a96747f3907ec684ce0d9c9980a2621cc65581913dec0  grommunio-virtual-mailbox-maps.cf
929f5a44d7a343c11e3a10e5d972e4ec2362f08126efa45fa61f1b70efd01a650df2aaff7140b39d7b88b4754cabc0fac3880dfdc2e91232b6f129fe4ad2326d  grommunio-bcc-forwards.cf
"
