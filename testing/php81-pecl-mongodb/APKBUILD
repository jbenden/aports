# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-mongodb
_extname=mongodb
pkgver=1.19.1
pkgrel=0
pkgdesc="PHP 8.1 MongoDB driver - PECL"
url="https://pecl.php.net/package/mongodb"
arch="all"
license="Apache-2.0"
depends="php81-common"
makedepends="cyrus-sasl-dev icu-dev openssl-dev>3 php81-dev snappy-dev zstd-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir"/$_extname-$pkgver
provides="php81-mongodb=$pkgver-r$pkgrel" # for backward compatibility
replaces="php81-mongodb" # for backward compatibility

build() {
	phpize81
	./configure --prefix=/usr \
		--with-php-config=/usr/bin/php-config81
	make
}

check() {
	# tests requires additional dependencies (vagrant)
	php81 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/php81/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
c2148bfdfc1bda8a352d162fd067ece870b11668f20ae70ccfefc60759cb50711dec16fb7d2f67aa21543542ded12c620b0062b20cf9f42df637377c96f3e64b  php-pecl-mongodb-1.19.1.tgz
"
